

## Prueba App-voyager

creación de una app de laravel con voyager basandonos en los tutoriales

- [Tutorial](https://voyager.devdojo.com/academy/media-manager/).

## prueba de la App
creación y configuración de vhost en tu computadora basado en el siguiente enlace:

- [enlace de vhost](https://desarrolloweb.com/articulos/configurar-virtual-hosts-apache-windows.html).

## crear a laravel app
para crear una app de laravel ir ala documentación del mismo 

- [enlace de documentación](https://laravel.com/docs/8.x).
 o ocupar el comando:

 composer create-project --prefer-dist laravel/nombre del proyecto
 
 *probar si funciona correctamente

 ## modificar .env

 modificar .env con la base creada y correro los comandos indicados.
 
   